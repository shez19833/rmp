<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Address;
use App\Models\Course;

// TODO could be joined up with Users and have a user type (student, api )or roles.
$factory->define(App\Models\Student::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'nationality' => $faker->nationality,
        'address_id' => function() {
            return Address::inRandomOrder()->first()->id;
        },
        'course_id' => function() {
            return Course::inRandomOrder()->first()->id;
        },
    ];
});
