<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Independent
        $this->call(AddressTableSeeder::class);
        $this->call(CourseTableSeeder::class);
        $this->call(UserTableSeeder::class);

        // Dependent
        $this->call(StudentTableSeeder::class);
    }
}
