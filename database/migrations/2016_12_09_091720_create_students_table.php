<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function ($table) {
            $table->increments('id')->unsigned();
            $table->text('first_name');
            $table->text('last_name');
            $table->text('email');
            $table->text('nationality'); // TODO should be a nationalities table
            $table->integer('address_id');
            $table->integer('course_id');
            $table->timestamps();

            $table->foreign('address_id')
                ->references('id')
                ->on('addresses');

            $table->foreign('course_id')
                ->references('id')
                ->on('courses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
