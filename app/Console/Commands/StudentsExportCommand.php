<?php

namespace App\Console\Commands;

use App\Mail\StudentExportFileReady;
use App\Services\StudentExportFileService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class StudentsExportCommand extends Command
{
    // TODO The name and signature of the console command.
    // TODO I always use z: so i can see all my commands at the bottom of
    // TODO console.
    /**
     *
     * @var string
     */
    protected $signature = 'z:export-all-students 
                        {email : Email to send report to}';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(StudentExportFileService $studentExportFileService)
    {
        $filePath = $studentExportFileService->getCSVFile();

        // TODO this is set to LOG so no emails will be sent but can be viewed in
        // TODO storage/logs/laravel.log
        Mail::to($this->argument('email'))
            ->send(new StudentExportFileReady($filePath));
    }
}
