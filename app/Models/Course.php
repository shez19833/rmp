<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * Get the Students for this Course
     */
    public function students()
    {
        return $this->hasMany(Student::class);
    }
}
