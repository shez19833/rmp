<?php

namespace App\Models;

use App\Models\StudentAddresses;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public $timestamps = false;

    protected $hidden = ['course_id', 'address_id', 'created_at', 'updated_at'];

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }
}
