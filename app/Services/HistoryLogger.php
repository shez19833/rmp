<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class HistoryLogger
{
    private $file = 'history.txt';

    public function write($type)
    {
        $content = now()->format('d/m/Y h:i') . '~~' . $type . " export\n";

        Storage::append($this->file, $content);
    }

    public function getAllHistory()
    {
        $contents = array_reverse( // latest first
            array_filter( // getting rid of empty new line at the end
                explode("\n", Storage::get($this->file))
            )
        );

        return $contents;
    }
}
