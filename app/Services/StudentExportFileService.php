<?php

namespace App\Services;

use App\Models\Student;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class StudentExportFileService {

    private $path = 'exports';

    private $logger;

    public function __construct(HistoryLogger $logger)
    {

        $this->logger = $logger;
    }

    public function getCSVFile()
    {
        $filePath = $this->path . '/' . Str::random() . '.csv';

        // TODO using direct Query as its faster than hydrating Eloquent for no beneficial
        // TODO reason in this case

        DB::table('students')
            ->join('courses', 'courses.id', '=', 'students.course_id')
            ->select('students.first_name', 'students.last_name', 'students.email', 'courses.name as course', 'courses.university')
            ->orderBy('students.id')
            ->chunk(200, function ($students) use ($filePath) {
                // TODO potentially could be DRYied up
                $studentsDataExtracted = $students->map(function($student) {
                    return implode(",", [
                        $student->first_name,
                        $student->last_name,
                        $student->email,
                        $student->course,
                        str_replace(',', '.',$student->university),
                    ]);
                });
                Storage::append($filePath, implode("\n", $studentsDataExtracted->toArray()));
        });

        $this->logger->write('full');
        return $filePath;
    }
}
