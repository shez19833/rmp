<?php

namespace App\Services;

use App\Models\Student;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class StudentExportService
{
    private $logger;

    public function __construct(HistoryLogger $logger)
    {

        $this->logger = $logger;
    }

    // TODO tested this with 20000 users seems to be quick
    public function getCSVDataForSelectedUsers(array $ids)
    {
        return $this->getCSVData($ids);
    }

    public function getCSVDataForAllUsers()
    {
        return $this->getCSVData();
    }

    private function getCSVData($ids = null)
    {
        $students = Student::with('course');

        if ($ids) {
            $students = $students->findMany($ids, ['first_name', 'last_name', 'email', 'course_id']);
            $this->logger->write('selected');
        } else {
            $students = $students->get(['first_name', 'last_name', 'email', 'course_id']);
            $this->logger->write('full');
        }

        return $students->map(function($student) {
            return implode(",", [
                $student->first_name,
                $student->last_name,
                $student->email,
                $student->course->name,
                str_replace(',', '.',$student->course->university),
            ]);
        });
    }
}
