<?php

namespace App\Http\Controllers;

use Bueltge\Marksimple\Marksimple;

class WelcomeController extends Controller
{
    public function __invoke() : \Illuminate\View\View
    {
        $ms = new Marksimple();

        return view('welcome', [
            'content' =>  $ms->parseFile('../README.md'),
        ]);
    }
}
