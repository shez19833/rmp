<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Services\HistoryLogger;

class HistoryController extends Controller
{
    /**
     * View all students found in the database
     */
    public function __invoke(HistoryLogger $logger) : \Illuminate\View\View
    {
        $history = $logger->getAllHistory();

        return view('history.index', compact('history'));
    }
}
