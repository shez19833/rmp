<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\StudentResource;
use App\Models\Student;
use Illuminate\Support\Facades\Auth;

class StudentsController extends Controller
{
    public function __construct()
    {
        // Only to test in the browser api auth
        Auth::loginUsingId(1);
    }

    /**
     * View all students found in the database
     */
    public function __invoke()
    {
        $students = Student::with('course')->paginate();

        return StudentResource::collection($students);
    }
}
