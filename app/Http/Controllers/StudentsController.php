<?php

namespace App\Http\Controllers;

use App\Models\Student;

class StudentsController extends Controller
{
    /**
     * View all students found in the database
     */
    public function __invoke() : \Illuminate\View\View
    {
        $students = Student::with('course')->paginate();

        return view('students.index', compact('students'));
    }
}
