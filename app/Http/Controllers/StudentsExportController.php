<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExportSelected;
use App\Services\StudentExportService;

class StudentsExportController extends Controller
{
    /**
     * Exports selected students data to a CSV file
     */
    public function __invoke(ExportSelected $request, StudentExportService $studentExportService)
    {
        $students = $request->has('exportAll')
            ? $studentExportService->getCSVDataForAllUsers()
            : $studentExportService->getCSVDataForSelectedUsers($request->studentId);

        return response()->streamDownload(function () use ($students) {
            echo implode("\n", $students->toArray());
        }, 'Students Export.csv');
    }
}
