<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Support\Facades\Auth;

class VueStudentsController extends Controller
{
    public function __construct()
    {
        // Only to test in the browser api auth
        Auth::loginUsingId(1);
    }

    /**
     * View all students found in the database
     */
    public function __invoke() : \Illuminate\View\View
    {
        return view('students.vue.index');
    }
}
