<?php

namespace App\Http\Controllers;

use App\Services\StudentExportFileService;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class StudentsLargeExportController extends Controller
{
    /**
     * Exports ALL students data to a CSV file
     */
    public function __invoke(StudentExportFileService $studentExportFileService)
    {
        $filePath = $studentExportFileService->getCSVFile();

        return response()
            ->download(Storage::path($filePath), 'Export.csv')
            ->deleteFileAfterSend();
    }
}
