<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StudentExportFileReady extends Mailable
{
    use Queueable, SerializesModels;

    private $filePath;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('student@exportservices.com')
            ->view('emails.exports.students')
            ->attachFromStorage($this->filePath, 'name.pdf');
    }
}
