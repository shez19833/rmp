<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Welcome to the task</title>
        <style>
            @import url(//fonts.googleapis.com/css?family=Lato:700);

            body {
                margin:0;
                font-family:'Lato', sans-serif;
                text-align:center;
                color: #999;
            }

            .header {
                width: 100%;
                left: 0px;
                top: 5%;
                text-align: left;
                border-bottom: 1px  #999 solid;
            }

            .student-table{
                width:100%;
            }

            table.student-table th{
                background-color: #C6C6C6;
                text-align: left;
                color: white;
                padding:7px 3px;
                font-weight: 700;
                font-size: 18px;
            }

            table.student-table tr.odd {
                text-align: left;
                padding:5px;
                background-color: #F9F9F9;
            }

            table.student-table td{
                text-align: left;
                padding:5px;
            }

            a, a:visited {
                text-decoration:none;
                color: #999;
            }

            h1 {
                font-size: 32px;
                margin: 16px 0 0 0;
            }
        </style>

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        @stack('styles')

        <script>
            window.Laravel = <?php echo json_encode([
                    'csrfToken' => csrf_token(),
//                    'api_token' => Auth::user()->api_token
            ]); ?>
        </script>
    </head>

    <body>
        <div class="container" id="app">
            <nav>
                <ul class="nav nav-tabs">
                    <li>
                        <a href="{{ route('home') }}">Home</a>
                    </li>
                    <li>
                        <a href="{{ route('students.index') }}">Students List</a>
                    </li>
                    <li>
                        <a href="{{ route('students.vue.index') }}">Students List (Vue)</a>
                    </li>
                    <li>
                        <a href="{{ route('history.index') }}">Exports History</a>
                    </li>
                </ul>
            </nav>

            <div><img src="/images/logo.png" alt="RMP Logo" title="RMP logo" width="100%"></div>

            @if (session('alert'))
                <div class="alert alert-warning">
                    {{ session('alert') }}
                </div>
            @elseif(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @yield('content')
        </div>

        <footer class="footer">
            <div class="container">
                <p class="text-muted">Code Test</p>
            </div>
        </footer>

    @stack('json')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('scripts')
    </body>
</html>
