@extends('layout.app')


@push('styles')
<link rel="stylesheet" href="{{ 'plugins/sweet-alert/sweetalert.css' }}">
@endpush

@section('content')
    <Students/>
@endsection

@push('scripts')
<script src="{{ 'plugins/sweet-alert/sweetalert.min.js' }}"></script>
@endpush
