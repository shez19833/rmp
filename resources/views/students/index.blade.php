@extends('layout.app')

@section('content')
<form action="{{ route('students.export.large') }}" method="post" id="student_form">
    {{ csrf_field() }}
    <input type="submit" name="exportAll" value="Export All (for Large Exports V2)"/>
</form>

<form action="{{ route('students.export.command') }}" method="post" id="student_form">
    {{ csrf_field() }}
    <input type="email" name="email" placeholder="email to send data to" required/>
    <input type="submit" name="exportAll" value="Export All (Command)"/>
</form>

<form action="{{ route('students.export.queue') }}" method="post" id="student_form">
    {{ csrf_field() }}
    <input type="email" name="email" placeholder="email to send data to" required/>
    <input type="submit" name="exportAll" value="Export All (Queue)"/>
</form>

<form action="{{ route('students.export') }}" method="post" id="student_form">
{{ csrf_field() }}

<div class="header">
    <div class="text-left mt-5">
        <input type="submit" name="exportAll" value="Export All"/>
        <input type="submit" name="exportSelected" value="Export Selected" id="submit" />
    </div>
</div>

<div class="text-center mt-5">
    {{ $students->render() }}
    <table class="student-table">
        <tr>
            <th></th>
            <th>Forename</th>
            <th>Surname</th>
            <th>Email</th>
            <th>University</th>
            <th>Course</th>
        </tr>
        @forelse ($students as $student)
            <tr>
                <td><input type="checkbox" class="checkbox" name="studentId[]" value="{{ $student['id'] }}"></td>
                <td>{{ $student['first_name'] }}</td>
                <td>{{ $student['last_name'] }}</td>
                <td>{{ $student['email'] }}</td>
                <td>{{ $student['course']['university'] }}</td>
                <td>{{ $student['course']['name'] }}</td>
            </tr>
        @empty
            <tr>
                <td class="text-center" colspan="6">Oh dear, no data found.</td>
            </tr>
        @endforelse
    </table>
</div>
</form>
@endsection

@push('scripts')
    <script src="{{ asset('js/checkbox.js') }}"></script>
@endpush
