@extends('layout.app')

@section('content')
    <ul>
        @foreach($history as $data)
        <li>
            {{ explode('~~', $data)[0] }} - {{ explode('~~', $data)[1] }}
        </li>
        @endforeach
    </ul>
@endsection
