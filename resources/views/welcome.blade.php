@extends('layout.app')

@section('content')
<div class="instructions">
    {!! $content !!}

    <p><a href="{{url('view')}}" title="task">Click here to take on the challenge</a></p>
</div>
@endsection
