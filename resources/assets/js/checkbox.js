$(document).ready(function() {

  var allChecked = false;

  $("#select_all").click(function(event){
    event.preventDefault();
    allChecked = !allChecked;
    $(".checkbox").prop('checked', allChecked);
    setSelectAllLabel(allChecked);
  });

  $('.checkbox').change(function(){
    if(false == $(this).prop("checked")){
      allChecked = false;
    }
    if ($('.checkbox:checked').length == $('.checkbox').length ){
      allChecked = true;
    }
    setSelectAllLabel(allChecked);
  });

  function setSelectAllLabel(allChecked) {
    if (allChecked) {
      $("#select_all").prop("value", "Unselect All");
    } else {
      $("#select_all").prop("value", "Select All");
    }
  }

});
