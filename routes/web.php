<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\Http\Controllers\HistoryController;
use App\Http\Controllers\StudentsController;
use App\Http\Controllers\StudentsExportController;
use App\Http\Controllers\StudentsLargeExportController;
use App\Http\Controllers\VueStudentsController;
use App\Http\Controllers\WelcomeController;
use App\Jobs\ProcessStudentExportToCSVFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;


Route::get('/', WelcomeController::class)->name('home');
Route::get('/students', StudentsController::class)->name('students.index');
Route::get('/history', HistoryController::class)->name('history.index');

// TODO in real life you wouldn't have three routes, you would pick one
// TODO I wasn't entirely sure on what you meant by large dataset so I changed it slightly
// TODO  large (all export) there is no NEED to check each students checkbox (this poses problem with PHP input)
// TODO  if there are millions of records the php max execution script would squak anyway
// TODO  is a CONSOLE command that would email you the file. (i am running this from HTTP but you could run it from console)
// TODO  displaying 2000k records was taking so long so I added pagination but this meant
// TODO  cant really select records from different pages .. (one solution would be to do some kind of criteria based searching)
// TODO  Although i have created a HTTP to trigger command, it should be done via CLI or Queues

Route::post('/students/export', StudentsExportController::class)->name('students.export');
Route::post('/students/export/large', StudentsLargeExportController::class)->name('students.export.large');

// TODO just doing it inline for quickness. i realise routes cannot be cached using this approach
Route::post('/students/export/command', function(Request $request) {
    Artisan::call('z:export-all-students', ['email' => $request->email]);
    return back()->with('success', 'You will receive the file shortly');
})->name('students.export.command');

// TODO wont work unless you turn on php artisan queue:work --queue=exports command
Route::post('/students/export/queue', function(Request $request) {
    ProcessStudentExportToCSVFile::dispatch($request->email)->onQueue('exports');
    return back()->with('success', 'You will receive the file shortly');
})->name('students.export.queue');

// Optional extra
Route::get('/vue/students', VueStudentsController::class)->name('students.vue.index');
