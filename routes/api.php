<?php

use App\Http\Controllers\Api\StudentsController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Optional extra
// Route::group(['middleware' => 'auth:api', 'prefix' => 'api'], function () {
     Route::get('students', StudentsController::class)->name('api.students.index');
//     Route::post('students', 'ExportController@exportStudentsToCsvWithVue');
//     Route::post('course-attendance', 'ExportController@exportCourseAttendenceToCsvWithVue');
// });
