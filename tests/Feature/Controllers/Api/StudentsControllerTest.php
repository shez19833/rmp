<?php

namespace Tests\Feature\Controllers\Api;

use App\Models\Address;
use App\Models\Course;
use App\Models\Student;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudentsControllerTest extends TestCase
{
    public function test_i_can_get_list_of_students()
    {
        $address = factory(Address::class)->create();
        $course = factory(Course::class)->create();

        $student1 = factory(Student::class)->create([
            'address_id' => $address->id,
            'course_id' => $course->id
        ]);

        $student2 = factory(Student::class)->create([
            'address_id' => $address->id,
            'course_id' => $course->id
        ]);

        $expected = [
            'id' => $student1->id,
            'email' => $student1->email,
            'university' => $student1->course->university,
            'course' =>  $student1->course->name,
        ];

        $this->get(route('api.students.index'))
            ->assertJsonFragment($expected);
    }
}
