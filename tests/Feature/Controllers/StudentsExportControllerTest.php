<?php

namespace Tests\Feature\Controllers;

use App\Models\Address;
use App\Models\Course;
use App\Models\Student;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudentsExportControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function test_i_can_export_selected_list_of_students()
    {
        $address = factory(Address::class)->create();
        $course = factory(Course::class)->create();

        $student1 = factory(Student::class)->create([
            'address_id' => $address->id,
            'course_id' => $course->id
        ]);

        $student2 = factory(Student::class)->create([
            'address_id' => $address->id,
            'course_id' => $course->id
        ]);

        $student3 = factory(Student::class)->create([
            'address_id' => $address->id,
            'course_id' => $course->id
        ]);

        $response = $this->post(route('students.export'), [
            '_token' => csrf_token(),
            'studentId' => [$student1->id, $student2->id]
        ]);
        $content = $response->streamedContent();

        $this->assertStringContainsString($student1->first_name, $content);
        $this->assertStringContainsString($student2->first_name, $content);
        $this->assertStringNotContainsString($student3->first_name, $content);
    }
}
