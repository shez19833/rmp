<?php

namespace Tests\Feature\Controllers;

use App\Models\Address;
use App\Models\Course;
use App\Models\Student;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudentsControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function test_i_can_see_a_list_of_students_with_courses()
    {
        $address = factory(Address::class)->create();
        $course = factory(Course::class)->create();

        $student1 = factory(Student::class)->create([
            'address_id' => $address->id,
            'course_id' => $course->id
        ]);

        $student2 = factory(Student::class)->create([
            'address_id' => $address->id,
            'course_id' => $course->id
        ]);

        $this->get(route('students.index'))
            ->assertSee($student1->first_name)
            ->assertSee($student2->first_name);
    }
}
