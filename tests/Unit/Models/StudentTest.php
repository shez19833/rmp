<?php

namespace Tests\Unit;

use App\Models\Address;
use App\Models\Course;
use App\Models\Student;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudentTest extends TestCase
{
    use DatabaseMigrations;

    public function test_i_can_get_student()
    {
        $address = factory(Address::class)->create();
        $course = factory(Course::class)->create();
        $student = factory(Student::class)->create([
            'address_id' => $address->id,
            'course_id' => $course->id
        ]);

        $this->assertEquals($student->toArray(), Student::first()->toArray());
    }

    public function test_i_can_get_course_related_to_a_student()
    {
        $address = factory(Address::class)->create();
        $course = factory(Course::class)->create();
        $students = factory(Student::class, 2)->create([
            'address_id' => $address->id,
            'course_id' => $course->id
        ]);

        $this->assertEquals($course->name,
            Student::with('course')->first()->course->name);
    }

    public function test_i_cannot_get_course_not_related_to_a_student()
    {
        $course1 = factory(Course::class)->create();

        $address = factory(Address::class)->create();
        $course2 = factory(Course::class)->create();
        factory(Student::class)->create([
            'address_id' => $address->id,
            'course_id' => $course2->id
        ]);
        $this->assertNotEquals($course1->name, Student::with('course')->first()->course->name);
    }
}
