<?php

namespace Tests\Unit;

use App\Models\Address;
use App\Models\Course;
use App\Models\Student;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CourseTest extends TestCase
{
    use DatabaseMigrations;

    public function test_i_can_get_course()
    {
        $course = factory(Course::class)->create();

        $this->assertEquals($course->toArray(), Course::first()->toArray());
    }

    public function test_i_can_get_related_student_associated_with_a_course()
    {
        $address = factory(Address::class)->create();
        $course = factory(Course::class)->create();
        $students = factory(Student::class, 2)->create([
            'address_id' => $address->id,
            'course_id' => $course->id
        ]);

        $this->assertEquals($students->pluck('first_name'),
            Course::with('students')->first()->students->pluck('first_name'));
    }

    public function test_i_cannot_get_an_unrelated_student_not_associated_with_a_course()
    {
        factory(Course::class)->create();

        $address = factory(Address::class)->create();
        $course2 = factory(Course::class)->create();
        factory(Student::class)->create([
            'address_id' => $address->id,
            'course_id' => $course2->id
        ]);
        $this->assertEmpty(Course::with('students')->first()->students);
    }
}
