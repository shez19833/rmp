<?php

namespace Tests\Unit;

use App\Models\Address;
use App\Models\Course;
use App\Models\Student;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AddressTest extends TestCase
{
    use DatabaseMigrations;

    public function test_i_can_get_address()
    {
        $address = factory(Address::class)->create();

        $this->assertEquals($address->toArray(), Address::first()->toArray());
    }

    public function test_i_can_get_related_students_of_an_address()
    {
        $address = factory(Address::class)->create();
        $course = factory(Course::class)->create();
        $students = factory(Student::class, 2)->create([
            'address_id' => $address->id,
            'course_id' => $course->id
        ]);

        $studentsForAddress = Address::with('students')->first()->students;

        $expected = [$students->first()->first_name, $students->last()->first_name];
        $actual = [$studentsForAddress->first()->first_name, $studentsForAddress->last()->first_name];

        $this->assertEquals($expected, $actual);
    }

    public function test_i_cannot_get_an_unrelated_student_of_an_address()
    {
        $course = factory(Course::class)->create();
        $address = factory(Address::class)->create();
        $address2 = factory(Address::class)->create();
        $student = factory(Student::class)->create([
            'address_id' => $address2->id,
            'course_id' => $course->id
        ]);

        $this->assertEmpty(Address::with('students')
            ->first()->students);
    }
}
